import { Scene } from 'phaser';
import {Game_Level2} from "./Game_Level2.ts";
import {Test} from "./Test.ts";

export class Preloader extends Scene
{
    constructor ()
    {
        super('Preloader');
    }

    init ()
    {
        
    }
    
    preload ()
    {
        // Загрузка изображений
        this.load.image('background', 'assets/bgFullHD.jpg');
        this.load.image('finish', 'assets/finish.png');
        this.load.image('delete', 'assets/delete.png');
        this.load.spritesheet('robot', 'assets/robot-up.png', { frameWidth: 100, frameHeight: 70 });
        this.load.spritesheet('robot_left', 'assets/robot-left.png', { frameWidth: 100, frameHeight: 70 });
        this.load.spritesheet('robot_right', 'assets/robot-right.png', { frameWidth: 100, frameHeight: 70 });
        this.load.spritesheet('robot_down', 'assets/robot-down.png', { frameWidth: 100, frameHeight: 70 });
        this.load.spritesheet('robot_stay_left', 'assets/robot_stay_left.png', { frameWidth: 100, frameHeight: 70 });
        this.load.spritesheet('robot_stay_right', 'assets/robot_stay_right.png', { frameWidth: 100, frameHeight: 70 });

        //изображения для победы
        this.load.image('finish_img1', 'assets/finish/1.jpg');
        this.load.image('finish_img2', 'assets/finish/2.jpg');
        this.load.image('finish_img3', 'assets/finish/3.jpg');
        this.load.image('finish_img4', 'assets/finish/4.jpg');
        this.load.image('finish_img5', 'assets/finish/5.jpg');
        
    }

    create ()
    {
        let startGame =  true;
        this.scene.start('WelcomeScene', {startGame});
    }
}
