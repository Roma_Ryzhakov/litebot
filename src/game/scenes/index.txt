import { EventBus } from '../EventBus';
import { Scene } from 'phaser';

export class Game_Level2 extends Scene {
    private background: Phaser.GameObjects.Image;
    private cellSize: number = 79; // Размер шага
    private fieldX: number = 297;
    private fieldY: number = 137;
    private fieldWidth: number = 560;
    private fieldHeight: number = 560;
    private finishX: number; // Координаты финишной клетки
    private finishY: number;

    private robot: Phaser.Physics.Arcade.Sprite;
    private finishImg: Phaser.GameObjects.Image;
    private moveQueue: string[] = [];
    private commandText: Phaser.GameObjects.Text;
    private currentDirection: string = 'up';
    private isExecuting: boolean = false;

    // Кнопки
    private robotButton: Phaser.GameObjects.Text;
    private moveButton: Phaser.GameObjects.Text;
    private stepButtons: Phaser.GameObjects.Text[] = [];
    private turnRightButton: Phaser.GameObjects.Text;
    private turnLeftButton: Phaser.GameObjects.Text;
    private startButton: Phaser.GameObjects.Text;
    private addNewLine: boolean = false;

    constructor() {
        super('Game_Level2');
    }

    create() {
        // Установка фона
        this.background = this.add.image(0, 0, 'background').setOrigin(0, 0);
        // Финальная клетка
        this.finishX = this.fieldX + this.cellSize * 2 + this.cellSize / 2; // Координаты финишной клетки
        this.finishY = this.fieldY + this.cellSize / 2;
        this.add.image(this.finishX, this.finishY, 'finish').setOrigin(0.5, 0.5);

        // Создание робота
        this.robot = this.physics.add.sprite(this.fieldX + this.cellSize * 2 + this.cellSize / 2, this.fieldY + this.cellSize * 5 + this.cellSize / 2, 'robot');
        this.robot.setCollideWorldBounds(true);
        this.robot.setOrigin(0.5, 0.5);

        this.anims.create({
            key: 'up',
            frames: [{ key: 'robot', frame: 0 }],
            frameRate: 10,
        });

        this.anims.create({
            key: 'down',
            frames: [{ key: 'robot_down', frame: 0 }],
            frameRate: 10,
        });

        this.anims.create({
            key: 'left',
            frames: [{ key: 'robot_left', frame: 0 }],
            frameRate: 10,
        });

        this.anims.create({
            key: 'right',
            frames: [{ key: 'robot_right', frame: 0 }],
            frameRate: 10,
        });

        // Установка границ области для робота
        this.physics.world.setBounds(this.fieldX, this.fieldY, this.fieldWidth, this.fieldHeight);
        this.robot.body.setCollideWorldBounds(true);
        this.robot.body.onWorldBounds = true;

        // Команды робота
        this.add.text(1000, 36, 'Команды робота:', { fontSize: '32px', fill: '#f3ff00' });
        this.commandText = this.add.text(1000, 100, '', { fontSize: '24px', fill: '#f3ff00' });

        // Создание кнопки "Robot"
        this.robotButton = this.add.text(1000, 600, 'Robot', { fontSize: '24px', fill: '#ffd500', stroke: '#ffea06', strokeThickness: 3 })
            .setInteractive()
            .on('pointerdown', () => this.showMoveButtons());

        this.startButton = this.add.text(1000, 850, 'Запустить', { fontSize: '24px', fill: '#4c0000', stroke: '#ff0606', strokeThickness: 3 })
            .setInteractive()
            .on('pointerdown', () => this.startMoveTasks());

        EventBus.emit('current-scene-ready', this);
    }

    showMoveButtons() {
        if (this.addNewLine === true) {
            this.commandText.setText(this.commandText.text + '\n' + 'Robot.');
        } else {
            this.commandText.setText(this.commandText.text + 'Robot.');
        }

        // Отключение кнопки "Robot"
        this.robotButton.disableInteractive();
        this.robotButton.setStyle({ fill: '#808080' });

        // Создание кнопки "Move"
        this.moveButton = this.add.text(1000, 650, 'Move', { fontSize: '24px', fill: '#ffd500', stroke: '#ffea06', strokeThickness: 3 })
            .setInteractive()
            .on('pointerdown', () => {
                this.showStepButtons();
                this.turnBtnDisable();
            });

        // Анимация появления кнопки "Move"
        this.tweens.add({
            targets: this.moveButton,
            alpha: { from: 0, to: 1 },
            duration: 500,
        });

        // Создание кнопок для поворотов
        this.turnRightButton = this.add.text(1100, 650, 'Right', { fontSize: '24px', fill: '#ffd500', stroke: '#ffea06', strokeThickness: 3 })
            .setInteractive()
            .on('pointerdown', () => {
                this.addMoveTask('right');
                this.hideMoveButtons();
                this.hideStepButtons();
                this.resetRobotButton();
                this.commandText.setText(this.commandText.text + 'right');
            });

        this.turnLeftButton = this.add.text(1250, 650, 'Left', { fontSize: '24px', fill: '#ffd500', stroke: '#ffea06', strokeThickness: 3 })
            .setInteractive()
            .on('pointerdown', () => {
                this.addMoveTask('left');
                this.hideMoveButtons();
                this.hideStepButtons();
                this.resetRobotButton();
                this.commandText.setText(this.commandText.text + 'left');
            });

        // Анимация появления кнопок поворотов
        this.tweens.add({
            targets: [this.turnRightButton, this.turnLeftButton],
            alpha: { from: 0, to: 1 },
            duration: 500,
        });
    }

    showStepButtons() {
        this.commandText.setText(this.commandText.text + 'move ');
        // Создание кнопок для выбора количества шагов
        for (let i = 1; i <= 6; i++) {
            const stepButton = this.add.text(970 + i * 40, 700, `${i}`, { fontSize: '24px', fill: '#ffd500', stroke: '#ffea06', strokeThickness: 3 })
                .setInteractive()
                .on('pointerdown', () => {
                    this.addMoveTask(`forward ${i}`);
                    this.hideStepButtons();
                    this.hideMoveButtons();
                    this.resetRobotButton();
                    // Добавляем только цифру в текст команд
                    this.commandText.setText(this.commandText.text + i);
                });

            this.stepButtons.push(stepButton);

            // Анимация появления кнопок
            this.tweens.add({
                targets: stepButton,
                alpha: { from: 0, to: 1 },
                duration: 500,
                delay: i * 100,
            });
        }
    }

    turnBtnDisable() {
        this.turnRightButton.disableInteractive();
        this.turnRightButton.setStyle({ fill: '#808080' });
        this.turnLeftButton.disableInteractive();
        this.turnLeftButton.setStyle({ fill: '#808080' });
        this.moveButton.disableInteractive();
        this.moveButton.setStyle({ fill: '#808080' });
    }

    resetRobotButton() {
        this.robotButton.setInteractive();
        this.robotButton.setStyle({ fill: '#ffd500' });
    }

    hideMoveButtons() {
        this.moveButton.destroy();
        this.turnRightButton.destroy();
        this.turnLeftButton.destroy();
    }

    hideStepButtons() {
        this.stepButtons.forEach(button => button.destroy());
        this.stepButtons = [];
    }

    addMoveTask(action: string) {
        this.moveQueue.push(action);
        this.addNewLine = true;
    }

    startMoveTasks() {
        if (!this.isExecuting && this.moveQueue.length > 0) {
            this.isExecuting = true;
            this.executeMoveTask();
        } else {
            console.log(`Нет задач`);
        }
    }

    executeMoveTask() {
        if (this.moveQueue.length === 0) {
            this.isExecuting = false;
            this.checkFinish();
            return;
        }

        const action = this.moveQueue.shift()!;
        const [command, steps] = action.split(' ');

        // В зависимости от действия выполняем нужное действие
        switch (command) {
            case 'forward':
                this.time.delayedCall(200, () => {
                    this.moveForward(parseInt(steps, 10));
                });
                break;
            case 'right':
                this.turnRight();
                break;
            case 'left':
                this.turnLeft();
                break;
            default:
                console.log(`Неизвестное действие: ${action}`);
        }

    }

    moveForward(steps: number) {
        // Двигаем робота в текущем направлении
        let deltaX = 0, deltaY = 0;

        switch (this.currentDirection) {
            case 'up':
                deltaY = -this.cellSize * steps;
                this.robot.anims.play('up', true);
                break;
            case 'right':
                deltaX = this.cellSize * steps;
                this.robot.anims.play('right', true);
                break;
            case 'down':
                deltaY = this.cellSize * steps;
                this.robot.anims.play('down', true);
                break;
            case 'left':
                deltaX = -this.cellSize * steps;
                this.robot.anims.play('left', true);
                break;
        }

        const newX = this.robot.x + deltaX;
        const newY = this.robot.y + deltaY;

        // Проверка, находится ли новое положение робота внутри границ
        if (newX >= this.fieldX + this.cellSize / 2 && newX <= this.fieldX + this.fieldWidth - this.cellSize / 2 &&
            newY >= this.fieldY + this.cellSize / 2 && newY <= this.fieldY + this.fieldHeight - this.cellSize / 2) {
            this.tweens.add({
                targets: this.robot,
                x: newX,
                y: newY,
                duration: 500 * steps,
                onComplete: () => {
                    this.robot.anims.stop();
                    this.executeMoveTask();
                }
            });
        } else {
            // Если новое положение вне границ, пропускаем эту задачу и переходим к следующей
            console.log('Робот не может двигаться в этом направлении, пропуск задачи.');
            this.executeMoveTask();
        }
    }

    turnRight() {
        // Поворачиваем робота направо
        switch (this.currentDirection) {
            case 'up':
                this.currentDirection = 'right';
                this.robot.anims.play('right', true);
                break;
            case 'right':
                this.currentDirection = 'down';
                this.robot.anims.play('down', true);
                break;
            case 'down':
                this.currentDirection = 'left';
                this.robot.anims.play('left', true);
                break;
            case 'left':
                this.currentDirection = 'up';
                this.robot.anims.play('up', true);
                break;
        }
        this.executeMoveTask(); // Запускаем следующую задачу после поворота
    }

    turnLeft() {
        // Поворачиваем робота налево
        switch (this.currentDirection) {
            case 'up':
                this.currentDirection = 'left';
                this.robot.anims.play('left', true);
                break;
            case 'right':
                this.currentDirection = 'up';
                this.robot.anims.play('up', true);
                break;
            case 'down':
                this.currentDirection = 'right';
                this.robot.anims.play('right', true);
                break;
            case 'left':
                this.currentDirection = 'down';
                this.robot.anims.play('down', true);
                break;
        }
        this.executeMoveTask(); // Запускаем следующую задачу после поворота
    }

    checkFinish() {
        const tolerance = this.cellSize / 4; // Допустимое отклонение для проверки
        const distance = Phaser.Math.Distance.Between(this.robot.x, this.robot.y, this.finishX, this.finishY);

        console.log(`Робот координаты: (${this.robot.x}, ${this.robot.y}), Финиш координаты: (${this.finishX}, ${this.finishY}), Расстояние: ${distance}`);

        if (distance < tolerance) {
            this.showModal('Вы выиграли!', tolerance, distance);
            this.moveQueue = [];
        } else {
            this.showModal('Похоже что ваш код не очень хорош! Попробуйте еще раз', tolerance, distance);
        }
    }

    showModal(message: string, tolerance: number, distance: number) {
        // Затемнение фона и остальные элементы модального окна создаются после задержки
        this.time.delayedCall(500, () => {
            // Затемнение фона
            let modalBackground = this.add.graphics();
            modalBackground.fillStyle(0x000000, 0.8);
            modalBackground.fillRect(0, 0, this.sys.game.config.width as number, this.sys.game.config.height as number);

            // Текст сообщения
            if (distance < tolerance) {
                this.finishImg = this.add.image(this.sys.game.config.width as number / 2, this.sys.game.config.height as number / 2 - 50, 'finish_img2').setOrigin(0.5, 0.5);
                let nextLevelButton = this.add.text(this.sys.game.config.width as number / 2, this.sys.game.config.height as number / 2 + 400, 'Следующий уровень', {
                    fontSize: '32px',
                    fill: '#013802',
                    backgroundColor: '#00ff0a',
                    padding: { x: 10, y: 5 }
                }).setOrigin(0.5, 0.5).setInteractive().on('pointerdown', () => {
                    this.changeScene();
                });
            } else {
                let modalText = this.add.text(this.sys.game.config.width as number / 2, this.sys.game.config.height as number / 2, message, {
                    fontSize: '48px',
                    fill: '#ffffff'
                }).setOrigin(0.5, 0.5);
                let restartButton = this.add.text(this.sys.game.config.width as number / 2, this.sys.game.config.height as number / 2 + 100, 'Попробовать еще раз', {
                    fontSize: '32px',
                    fill: '#ffffff',
                    backgroundColor: '#ff0000',
                    padding: { x: 10, y: 5 }
                }).setOrigin(0.5, 0.5).setInteractive().on('pointerdown', () => {
                    this.restartGame();
                });
            }
        }, [], this);
    }

    restartGame() {
        this.time.delayedCall(1000, () => {
            this.scene.restart();
            this.moveQueue = [];
            this.commandText.setText('');
            this.addNewLine = false;
            this.currentDirection;
        }, [], this);
    }

    changeScene() {
        this.scene.start('Game_Level2');
    }
}
